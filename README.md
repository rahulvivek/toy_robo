### Setup and Running

1. Create a new virtualenv named env using following command ( provide python 3 )
    
    ```virtualenv -p python3 env```

2. Activate env
    
    ```source env/bin/activate```

2. Install all dependency using following command.
    
    ```pip install -r requirements.txt```

3. Run the app using the following command.
    
    ```python app.py```
    
### API Docs


Sample POST Request

http://localhost:5000/instruction

Input JSON
```json
{
    "instruction": "PLACE 1,2,EAST MOVE MOVE LEFT MOVE REPORT"
}
```

Output JSON
```json
{ "output": "3 3 NORTH" }
```
