from flask import  Flask, request, jsonify

app = Flask(__name__)

class InstructionParser(object):
    """
    Parser to parse the instruction provied.
    It will create the result.
    """
    instruction = ''
    instructions = []
    result = []

    table_dimension = {
        'width': 5,
        'length': 5
    }

    origin = {
        'x': 0, 'y': 0
    }

    output = ' '
    current_position = None
    phase_direction = None
    is_report = False
    position_invalid = False
    is_robot_missing = False
    cannot_move = False

    def __init__(self, instruction_string):
        """
        When creating the parser need to pass the
        instruction string.
        :param instruction_string:
        """
        self.instructions = instruction_string.split(" ")

        self.parser()

    def parser(self):
        """
        Main Logic of the parser.
        Loop through the instructions
        will handle the exceptions and validations.
        :return: None
        """
        if self.instructions[0] != 'PLACE':
            # If PLACE is not provied we can ignore the next Instruction
            self.current_position = None
            self.is_robot_missing = True

        index = 0

        while index < len(self.instructions):

            current_instruction = self.instructions[index]

            if current_instruction == 'PLACE':

                try:

                    position_instruction = self.instructions[index + 1].split(',')
                    position = {
                        'x': int(position_instruction[0]),
                        'y': int(position_instruction[1])
                    }

                    self.phase_direction = position_instruction[2]
                    self.current_position = position

                    if self.validate_position():

                        index = index + 2
                        continue

                    else:

                        self.position_invalid = True
                        self.result = ['Position Missing']

                except:

                    self.result = ['Position not provided']

            elif current_instruction == 'MOVE':
                self.move()
            elif current_instruction == 'LEFT':
                self.left()
            elif current_instruction == 'RIGHT':
                self.right()
            elif current_instruction == 'REPORT':
                self.is_report = True
                self.report()

            index += 1

    def get_output(self):
        """
        Method to get the output string.
        :return: Str
        """
        return self.output

    def validate_position(self):
        """
        Validate the current position with the default table length and width
        :return: Bool
        """
        if (self.current_position['x'] <= self.table_dimension['width']
                and self.current_position['y'] <= self.table_dimension['length']):
            return True
        return False

    def left(self):
        """
        Change the direction when turing left
        :return: None
        """
        direction_map = {
            'NORTH': 'WEST',
            'WEST': 'SOUTH',
            'SOUTH': 'EAST',
            'EAST': 'NORTH'
        }

        self.phase_direction = direction_map[self.phase_direction]

    def right(self):
        """
        Change the direction when turning right
        :return: None
        """
        direction_map = {
            'NORTH': 'EAST',
            'EAST': 'SOUTH',
            'SOUTH': 'WEST',
            'WEST': 'NORTH'
        }

        self.phase_direction = direction_map[self.phase_direction]

    def move(self):
        """
        Increase of decrease the position value based on the phase direction.
        :return: None
        """
        if self.phase_direction == 'NORTH':
            self.current_position['y'] += 1

        elif self.phase_direction == 'SOUTH':
            self.current_position['y'] -= 1

        elif self.phase_direction == 'EAST':
            self.current_position['x'] += 1

        elif self.phase_direction == 'WEST':
            self.current_position['x'] -= 1

        if not self.validate_position():
            self.cannot_move = True

    def report(self):
        """
        Setting the out put string from the result array.
        :return:
        """
        if self.position_invalid:
            self.result = ["OUT OF TABLE"]
        elif self.is_robot_missing:
            self.result = ['ROBOT MISSING']

        elif self.cannot_move:
            self.result = ['OUT OF TABLE']

        elif self.current_position:
            self.result = [str(self.current_position['x']), str(self.current_position['y']), self.phase_direction]

        self.output = " ".join(self.result)


@app.route('/instruction', methods=['POST'])
def instruction():
    """
    View to handle the instruction.
    :return:
    """

    # Check the JSON Data
    try:
        data = request.json
        instruction_string = data.get('instruction', None)

    except Exception as e:
        return jsonify({
            'error': "Input JSON is missing"
        })

    if instruction_string:

        parser = InstructionParser(instruction_string)
        result = parser.get_output()

        return jsonify({
            'output': result
        })

    else:

        return jsonify({
            'error': 'No instruction given'
        })


if __name__ == '__main__':
    app.run(debug=True)


